import React from 'react'
import { Box } from '@material-ui/core'
import Grid from '@material-ui/core/Grid';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { makeStyles } from '@material-ui/core/styles'
import Button from '../Component/Button';
import RadioButton from '../Component/RadioButton';
import { fadeInUp } from 'react-animations';
import styled, { keyframes } from 'styled-components';

const bounceAnimation = keyframes`${fadeInUp}`;

const BouncyDiv = styled.div`
  animation: 1s ${bounceAnimation};
  display: flex;
`;

const useStyles = makeStyles(theme => ({
    title: {
        fontSize: 24,
        lineHeight: '32px',
        fontFamily: 'sans serif',
        marginBottom: 30
    },
    questionContainer: {
        color: 'white',
    }
}))

function Step5({ changeIndex }) {
    const classes = useStyles()
    return (
        <Box m="auto">
            <Grid item xs={10} sm={8} className={classes.questionContainer}>
                <BouncyDiv>
                    <div style={{
                        display: 'flex',
                        fontSize: 20,
                    }}>
                        <span style={{ marginTop: 6, color: 'rgb(217, 226, 241)' }}>
                            5
                        </span>
                        <ArrowForwardIcon style={{ marginTop: 8, fontSize: 18 }} />
                    </div>
                    <div style={{
                        marginLeft: 15
                    }}>
                        <div className={classes.title}>
                            How can USDAO help you ?
                        </div>
                        <div>
                            <RadioButton iconText="A" label="Integrate your business with USDAO ecosystem" fullWidth={true} />
                        </div>
                        <div>
                            <RadioButton iconText="B" label="Partner with us" fullWidth={true} />
                        </div>
                        <div>
                            <RadioButton iconText="C" label="Technical support" fullWidth={true} />
                        </div>
                        <div>
                            <RadioButton iconText="D" label="Other" fullWidth={true} />
                        </div>
                        <div>
                            <Button onPress={() => changeIndex(5)} />
                        </div>
                    </div>
                </BouncyDiv>
            </Grid>

        </Box>
    )
}

export default Step5
