import React from 'react'
import { Box } from '@material-ui/core'
import LinearProgress from '@material-ui/core/LinearProgress';
import Step1 from './Steps/Step1';
import './App.css';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Step2 from './Steps/Step2';
import Step3 from './Steps/Step3';
import Step4 from './Steps/Step4';
import Step5 from './Steps/Step5';
import Step6 from './Steps/Step6';

function App() {
  const [index, setIndex] = React.useState(0);

  const changeIndex = (i) => {
    setIndex(i);
  }

  return (
    <div className="App">
      <Box>
        <LinearProgress variant="determinate" value={10} />
      </Box>
      <Box
        display="flex"
        width="100%" height="100vh"
        style={{
          overflow: 'hidden'
        }}
      >
        {index == 0 ? <Step1 changeIndex={changeIndex} /> : index == 1 ? <Step2 changeIndex={changeIndex} /> : index == 2 ? <Step3 changeIndex={changeIndex} /> : index == 3 ? <Step4 changeIndex={changeIndex} /> : index == 4 ? <Step5 changeIndex={changeIndex} /> : <Step6 changeIndex={changeIndex} />}
      </Box>
      <Box style={{
        position: 'absolute',
        bottom: 20,
        right: 20
      }}>
        <ButtonGroup disableElevation variant="contained" color="primary">
          <Button style={{
            backgroundColor: 'rgb(0, 92, 249)',
            boxShadow: 'rgb(0 0 0 / 10%) 0px 3px 12px 0px',
          }}>
            <KeyboardArrowUpIcon />
          </Button>
          <Button style={{
            backgroundColor: 'rgb(0, 92, 249)',
            boxShadow: 'rgb(0 0 0 / 10%) 0px 3px 12px 0px',
          }}>
            <KeyboardArrowDownIcon />
          </Button>
        </ButtonGroup>
      </Box>
    </div>
  );
}

export default App;
